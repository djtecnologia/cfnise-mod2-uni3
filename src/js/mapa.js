var mapa = {
  name: 'Curso de Formação em Nível Inicial para Secretários Escolares',
  selected: true,
  unidade_ind: 0,
  range: [0, 69],
  children: [
    {
      name: 'Módulo 2 - Unidade 3: Redação Oficial',
      selected: false,
      unidade_ind: 0,
      range: [0, 69],
      children: [
        {
          name: 'Apresentação',
          selected: false,
          unidade_ind: 0,
          range: [0, 1]
        },
        {
          name: 'Gestão Documental',
          selected: false,
          unidade_ind: 0,
          range: [2, 11]
        },
        {
          name: 'Aspectos gerais da redação oficial  ',
          selected: false,
          unidade_ind: 0,
          range: [12, 21]
        },
        {
          name: 'Documentos de Arquivo',
          selected: false,
          unidade_ind: 0,
          range: [22, 29]
        },
        {
          name: 'Atos Oficiais e Comunicação Oficial',
          selected: false,
          unidade_ind: 0,
          range: [30, 54]
        },
        {
          name: 'Noções Básicas De Conservação Documental',
          selected: false,
          unidade_ind: 0,
          range: [55, 60]
        },
        {
          name: 'Atividade',
          selected: false,
          unidade_ind: 0,
          range: [61, 64]
        },
        {
          name: 'Encerramento',
          selected: false,
          unidade_ind: 0,
          range: [65, 65]
        },

      ]
    }
  ]
};

// define the item component
Vue.component('item', {
  template: '#item-template',
  props: {
    model: Object
  },
  data: function () {
    return {
      open: false
    }
  },
  computed: {
    isFolder: function () {
      return this.model.children &&
        this.model.children.length
    },
    selected: function () {
      return this.model.selected;
    },
    open: function () {
      return this.model.selected === true;
    }
    //  if (this.isFolder) {
    //    sel = false;
    //    this.model.children.forEach(function (element, index, array) {
    //      if (element.name.toLowerCase.indexOf(App.unidade.toLowerCase()) !== -1) {
    //        sel = true;
    //      }
    //    });
    //    return sel;
    //
    //  } else {
    //    return this.model.name.toLowerCase.indexOf(App.unidade.toLowerCase()) !== -1
    //  }
    //}
  },
  watch: {
    model: {
      handler: function (val, oldVal) {
        console.log('a thing changed')
      },
      deep: true
    }
  },
  methods: {
    toggle: function () {
      if (this.isFolder) {
        if (this.model.selected === true) {
          this.model.selected = false;
        } else {
          this.model.selected = true;
        }
      } else {
        if (App.pagina_last > this.model.range[0]) {
          App.goTo(this.model.unidade_ind, this.model.range[0]);
        }
        App.showLeft = false;
      }
    },
    changeType: function () {
      if (!this.isFolder) {
        Vue.set(this.model, 'children', [])
        this.addChild();
        this.open = true
      }
    },
    addChild: function () {
      //this.model.children.push({
      //  name: 'new stuff'
      //})
    }

  }
});
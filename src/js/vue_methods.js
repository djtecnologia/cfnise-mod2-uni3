function openMapa() {
  checkSel();
  this.showLeft = true;
}

function sbox(box, event, destaque) {
  destaque = destaque || 'pulse';
  element = $(event.target);
  // se tiver que obeceder a ordem
  if (box <= this.sb.order || !this.sb.keep_order) {

    //desativa todos os botoes
    element.siblings().removeClass('active');
    $('.active').removeClass('active');
    // ativa o botao clicado e remove a animacao
    element.addClass('active').removeClass('animated');

    this.sb.box = box;
    console.log(this.sb);
    if (box == this.sb.order && this.sb.order < this.sb.num_sb) {
      element.next().addClass('animated l-inf ' + destaque);
      this.sb.order++;
    } else {
      this.pode_navegar = true;
    }
  }
}

function sboxKeep(box, event, destaque) {
  destaque = destaque || 'pulse';
  element = $(event.target);
  // se tiver que obeceder a ordem
  console.log(this.sbk.box,this.sbk.order,box,this.sbk.order==box);

  if (box == this.sbk.order || !this.sbk.keep_order) {

    element.addClass('active');
    // ativa o botao clicado e remove a animacao
    element.removeClass('animated');

    this.sbk.box[box] = true;
    console.log(this.sbk.box);
    if (box == this.sbk.order && this.sbk.order < this.sbk.num_sb) {
      element.next().addClass('animated l-inf ' + destaque);
      this.sbk.order++;
    } else {
      this.pode_navegar = true;
    }
  }
}
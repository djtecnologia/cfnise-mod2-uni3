function keyNavegation() {


  $('#aumentar-fontes').on('click', function () {
    var zoom = document.body.style.zoom;
    if (zoom == '') {
      zoom = '100%';
    }
    var next_zoom = '';
    switch (zoom) {

      case '100%':
        next_zoom = '110%';
        break;
      case '110%':
        next_zoom = '125%';
        break;
      case '125%':
        next_zoom = '150%';
        break;
      case '150%':
        next_zoom = '175%';
        break;
      case '175%':
        next_zoom = '200%';
        break;
      case '200%':
        next_zoom = '200%';
        break;
    }
    document.body.style.zoom = next_zoom;
  });

  $('#diminuir-fontes').on('click', function () {
    var zoom = document.body.style.zoom;
    if (zoom == '') {
      zoom = '100%';
    }
    var next_zoom = '';
    switch (zoom) {
      case '100%':
        next_zoom = '100%';
        break;
      case '110%':
        next_zoom = '100%';
        break;
      case '125%':
        next_zoom = '110%';
        break;
      case '150%':
        next_zoom = '125%';
        break;
      case '175%':
        next_zoom = '150%';
        break;
      case '200%':
        next_zoom = '175%';
        break;
    }
    document.body.style.zoom = next_zoom;
  });
  $(document).keydown(function (event) {
    if (event.ctrlKey == true && (event.which == '107' || event.which == '109' || event.which == '187' || event.which == '189')) {
      event.preventDefault();
    }
  });

  var listener = new window.keypress.Listener();


  listener.simple_combo("ctrl =", function () {
    $('#aumentar-fontes').trigger('click');
  });

  listener.simple_combo("ctrl +", function () {
    $('#aumentar-fontes').trigger('click');
  });

  listener.simple_combo("ctrl -", function () {
    $('#diminuir-fontes').trigger('click');
  });

  listener.simple_combo("f2", function () {
    changePage("navegacao/guia-aprendiz.html", 1);
  });

  listener.simple_combo("f4", function () {
    alert("Glossario");
  });

  listener.simple_combo("f9", function () {
    $('#mapa-curso').modal('toggle');
  });

  $(document).on("click", '#mapa', function () {
    $('#mapa-curso').modal('toggle');
  });

  listener.simple_combo("ctrl 8", function () {
    if (!$("#proximo").hasClass("disabled")) {
      changePage($("#proximo").attr('data-route'), $("#proximo").attr('data-idp'));
    }
  });

  listener.simple_combo("ctrl *", function () {
    if (!$("#proximo").hasClass("disabled")) {
      changePage($("#proximo").attr('data-route'), $("#proximo").attr('data-idp'));
    }
  });

  listener.simple_combo("shift *", function () {
    if (!$("#anterior").hasClass("disabled")) {
      changePage($("#anterior").attr('data-route'), $("#anterior").attr('data-idp'));
    }
  });

  $(document).on("click", "#retornar", function () {
    changePage($("#voltando").val(), 1);
  });

  $(document).on("click", '#guia-aprendiz', function () {
    changePage("navegacao/guia-aprendiz.html", 1);
  });

}
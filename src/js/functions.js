Array.prototype.randomElement = function () {
  return this[Math.floor(Math.random() * this.length)]
};
$.fn.extend({
  animateCss: function (animationName, callback) {
    callback = callback || function () {
      };
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $(this).addClass('animated ' + animationName);
    $(this).one(animationEnd, function () {
      $(this).removeClass('animated ' + animationName);
      callback();
    });

  }
});

function checkSel() {
  mapa.children.forEach(function (nv1, i, a) {
    var is_sel = false;
    if (nv1.unidade_ind == App.unidade_ind) {
      mapa.children[i].selected = true;
      App.titulo1 = nv1.name;
      nv1.children.forEach(function (nv2, j, ar) {
        if (nv2.range[0] <= App.pagina_ind && nv2.range[1] >= App.pagina_ind) {
          mapa.children[i].children[j].selected = true;
          App.titulo2 = nv2.name;
        } else {
          mapa.children[i].children[j].selected = false;
        }
      });
    }
  });
}

function runAnimations(app, classe, order) {
  console.log('runAnimations', classe, order);
  if (order !== 0) {
    order = order || 1;
  }
  classe = classe || '';
  var group = '.' + classe + '_' + order;
  var prop = classe + '_' + order;
  console.log(classe, order, group, prop);

  if ($(group).length > 0) {
    order += 1;
    var next_group = '.' + classe + '_' + (order );
    var callbackfunc = function () {
      app.process();
    };
    if ($(next_group).length > 0) {
      if (app.tela[prop].navegacao != 'undefined') {
        callbackfunc = function () {
          runAnimations(app, classe, order);
        };
      } else {
        callbackfunc = function () {
          runAnimations(app, classe, order);
        };
      }

    }
    app.tela[prop].show = true;
    if (Array.isArray(app.tela[prop].anim)) {
      app.tela[prop].anim.forEach(function (item, index) {
        var sub_group = group + '_' + index;
        if (index == app.tela[prop].anim.length - 1) {
          $(sub_group).animateCss(item, callbackfunc);
        } else {
          $(sub_group).animateCss(item, function () {
          });
        }
      });
    } else {
      $(group).each(function (index, element) {
        if (index == $(group).length - 1) {
          console.log('call callbackfunc', callbackfunc);
          $(this).animateCss(app.tela[prop].anim, callbackfunc);
        } else {
          $(this).animateCss(app.tela[prop].anim);
        }
      });
    }
  } else {
  }
}

function randomAnim() {
  var animacoes = [
    'bounce',
    'flash',
    'pulse',
    'rubberBand',
    'shake',
    'headShake',
    'swing',
    'tada',
    'wobble',
    'jello',
    'bounceIn',
    'bounceInDown',
    'bounceInLeft',
    'bounceInRight',
    'bounceInUp',
    'bounceOut',
    'bounceOutDown',
    'bounceOutLeft',
    'bounceOutRight',
    'bounceOutUp',
    'fadeIn',
    'fadeInDown',
    'fadeInDownBig',
    'fadeInLeft',
    'fadeInLeftBig',
    'fadeInRight',
    'fadeInRightBig',
    'fadeInUp',
    'fadeInUpBig',
    'fadeOut',
    'fadeOutDown',
    'fadeOutDownBig',
    'fadeOutLeft',
    'fadeOutLeftBig',
    'fadeOutRight',
    'fadeOutRightBig',
    'fadeOutUp',
    'fadeOutUpBig',
    'flipInX',
    'flipInY',
    'flipOutX',
    'flipOutY',
    'lightSpeedIn',
    'lightSpeedOut',
    'rotateIn',
    'rotateInDownLeft',
    'rotateInDownRight',
    'rotateInUpLeft',
    'rotateInUpRight',
    'rotateOut',
    'rotateOutDownLeft',
    'rotateOutDownRight',
    'rotateOutUpLeft',
    'rotateOutUpRight',
    'hinge',
    'rollIn',
    'rollOut',
    'zoomIn',
    'zoomInDown',
    'zoomInLeft',
    'zoomInRight',
    'zoomInUp',
    'zoomOut',
    'zoomOutDown',
    'zoomOutLeft',
    'zoomOutRight',
    'zoomOutUp',
    'slideInDown',
    'slideInLeft',
    'slideInRight',
    'slideInUp',
    'slideOutDown',
    'slideOutLeft',
    'slideOutRight',
    'slideOutUp'
  ];

  return animacoes.randomElement();
}

function randomAnimEntrance() {
  var animacoes = [
    'bounceIn',
    'bounceInDown',
    'bounceInLeft',
    'bounceInUp',
    'fadeIn',
    'fadeInDown',
    'fadeInDownBig',
    'fadeInLeft',
    'fadeInLeftBig',
    'fadeInUp',
    'fadeInUpBig',
    'flipInX',
    'flipInY',
    'rotateIn',
    'rotateInDownLeft',
    'rotateInDownRight',
    'rotateInUpLeft',
    'rotateInUpRight',
    'rollIn',
    'zoomIn',
    'zoomInDown',
    'zoomInLeft',
    'zoomInRight',
    'zoomInUp',
    'slideInDown',
    'slideInLeft',
    'slideInUp'
  ]
  return animacoes.randomElement();
}

function switchValue(property, value, delay) {
  App.tela[property] = "";
  setTimeout(function () {
    App.tela[property] = value;
  }, delay);
}

function moveToCenter(drop_el, drag_el) {
  var left_end = (drop_el.position().left + ((drop_el.outerWidth() - drag_el.outerWidth()) / 2));//- (drag_el.left + (ui.draggable.width() / 2));
  var top_end = (drop_el.position().top + ((drop_el.outerHeight() - drag_el.outerHeight()) / 2));
  drag_el.animate({
    top: top_end,
    left: left_end
  }, 100);
}

function moveOrg(el) {
  el.animate({
    top: el.data('orgTop'),
    left: el.data('orgLeft')
  }, 100);
}

function saveOrg(el) {
  var left = el.position().left;
  var top = el.position().top;
  el.data('orgTop', top);
  el.data('orgLeft', left);
}
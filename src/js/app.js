//Vue.config.debug = true;
//Vue.config.async = false;
Vue.transition('FadeTeste', {
  enterClass: 'fadeIn-enter',
  leaveClass: 'fadeOut-leave'
});
var App = new Vue({
  el: function () {
    return '#app'
  },
  components: {
    progressbar: VueStrap.progressbar,
    aside: VueStrap.aside,
    vSelect: VueStrap.select
  },
  data: function () {
    return {
      showLeft: false,
      treeData: mapa,
      contraste_ativo: false,
      titulo1: '',
      titulo2: '',
      estrutura: estrutura,
      unidade_ind: 0,
      unidade_obj: {},
      pagina_ind: 0,
      pagina_last: 0,
      pagina_obj: {},
      showTela: false,
      showGuia: 'none',
      showMapa: false,
      showTelaConteudo: true,
      contentTela: '',
      tituloPagina: '',
      pode_navegar: false,
      biblioteca: 'none',
      tela: {},
      chunk: {},
      sb: {
        box: 0,
        order: 0,
        num_sb: 0,
        keep_order: true
      },
      sbk: {
        box: {},
        order: 0,
        num_sb: 0,
        keep_order: true
      }
    }
  },
  ready: function () {
    keyNavegation();
    carregaTempo();

    obj = GetBookmark();
    this.unidade_ind = obj.unidade;
    this.pagina_ind = obj.pagina;

    this.loadPage();
  },
  computed: {
    sel_pages: function () {
      var list = [];
      if (this.unidade_obj.paginas) {
        for (i = 0; i < this.pagina_last; i++) {
          list.push({'value': i, 'label': (i + 1) + ' de ' + this.unidade_obj.paginas.length});
        }
      }
      return list;
    },
    porcentagem: function () {
      if (this.unidade_obj.paginas) {
        return Math.round(((this.pagina_last) / this.unidade_obj.paginas.length) * 100);
      }
      return 0;
    },
    pode_avancar: function () {
      if (this.pagina_ind < this.pagina_last - 1) {
        return !(this.pagina_ind == this.pagina_max);
      }
      return this.pode_navegar && !(this.pagina_ind == this.pagina_max);
    },
    pode_voltar: function () {
      return !(this.pagina_ind == 0);
    },
    pagina_qtd: function () {
      if (this.unidade_obj.paginas) {
        return this.unidade_obj.paginas.length;
      }
    },
    pagina_max: function () {
      if (this.unidade_obj.paginas) {
        return this.unidade_obj.paginas.length - 1;
      }
    }
  },
  methods: {
    openMapa: openMapa,
    sbox: sbox,
    sboxKeep: sboxKeep,
    init: function () {
      console.log('init');
    },
    process: function () {

    },

    voltar: function (force) {
      if (this.pode_voltar || force) {
        this.pagina_ind -= 1;
        this.changePage();
      }
    },
    avancar: function (force) {
      if (this.pode_avancar || force) {
        this.pode_navegar = false;
        this.pagina_ind += 1;
        this.changePage();
      }
    },

    goTo: function (ui, pi) {
      console.log(ui, pi);
      this.unidade_ind = ui;
      this.pagina_ind = pi;
      this.changePage();
    },

    changePage: function () {
      SetBookmark(this.unidade_ind, this.pagina_ind);

      App.process = function () {
      };
      App.checkNav = function () {
        return false;
      };
      this.showTela = false;
    },
    loadPage: function () {
      this.unidade_obj = this.estrutura[this.unidade_ind];
      this.pagina_obj = this.unidade_obj.paginas[this.pagina_ind];
      this.$http.get(this.pagina_obj.file).then(function (response) {
        this.showTela = true;
        this.contentTela = response.text();
        $('#tela').html(this.contentTela);
        this.$compile($('#app').get(0));
        console.log('GARREGOURR');
      }, function () {
        // ERRO
      });

    },
    initPage: function () {
      checkSel();
      $('#tela').html(this.contentTela);
      this.pagina_last = atualizaPorcentagem(this.unidade_ind, this.pagina_ind) + 1;
      this.$compile($('#app').get(0));
      this.showTelaConteudo = true;
      this.pode_navegar = false;

      this.tituloPagina = this.pagina_obj.title;
    },
    startPage: function () {
      console.log('start');
      this.teste = true;
      this.init();
    },
    stopPage: function () {
      console.log('stop');
    },
  }
});

Vue.transition('mudarTela', {
  enterClass: entradaDefault,
  leaveClass: saidaDefault,
  afterEnter: function (el) {
    console.log('enter');
    App.initPage();
  },
  afterLeave: function (el) {
    console.log('leave');
    App.loadPage();
  }
});

Vue.transition('entradaTela', {
  enterClass: entradaDefault,
  leaveClass: saidaDefault,
  afterEnter: function (el) {
    console.log('enter-tela');
    App.startPage();
  },
  beforeLeave: function (el) {
    console.log('leave-tela');
    App.stopPage();
  },
  afterLeave: function () {
    console.log('aftar-leave-tela');
  }

});



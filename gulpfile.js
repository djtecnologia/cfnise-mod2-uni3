var path_scorm_base = 'scorm_base_dj';
var scorm_name = 'SMERJ-MOD2-UNI3';

// Include Gulp
var gulp = require('gulp');

// Include plugins
var plugins = require("gulp-load-plugins")({
  pattern: [
    'gulp-*',
    'gulp.*',
    'main-bower-files'
  ],
  replaceString: /\bgulp[\-.]/
});

// Define default destination folder
var dest = 'dist/';
var src = 'src/';

var overrides = {
  'bootstrap': {
    "main": [
      "less/bootstrap.less",
      "dist/css/bootstrap.css",
      "dist/fonts/glyphicons-halflings-regular.eot",
      "dist/fonts/glyphicons-halflings-regular.svg",
      "dist/fonts/glyphicons-halflings-regular.ttf",
      "dist/fonts/glyphicons-halflings-regular.woff"
    ]
  },
  'font-awesome': {
    "main": [
      "css/font-awesome.css",
      "fonts/**/*"
    ]
  }
};

// JS
gulp.task('js', function () {

  var jsFiles = ['src/js/*'];
  console.log(plugins.mainBowerFiles({overrides: overrides}));
  return gulp.src(plugins.mainBowerFiles({overrides: overrides}).concat(jsFiles))
    .pipe(plugins.plumber())
    .pipe(plugins.filter('**/*.js'))
    .pipe(plugins.order([
      'jquery.js',
      'jquery-ui.js',
      'urlobject.js',
      'scorm-constants.js',
      'scorm.js',
      'vue.js',
      'keypress-2.1.3.min.js',
      'vue-resource.js',
      'estrutura.js',
      'bootstrap.min.js',
      'mapa.js',
      'transition.js',
      'vue-strap.js',
      'vue_methods.js',
      '*'
    ]))
    .pipe(plugins.concat('main.js'))
    //.pipe(plugins.uglify() )
    .pipe(gulp.dest(dest + 'js'));
});

//  CSS
gulp.task('css', function () {
  var cssFiles = ['src/css/*', 'bower_components/vue-animate/dist/vue-animate.css'];
  return gulp.src(plugins.mainBowerFiles({overrides: overrides}).concat(cssFiles))
    .pipe(plugins.plumber())
    .pipe(plugins.filter('**/*.css'))
    .pipe(plugins.concat('main.css'))
    .pipe(plugins.cleanCss({debug: true}, function (details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(gulp.dest(dest + 'css'));
});

// Compile CSS from Sass files
gulp.task('sass', function () {
  return gulp.src('src/css/**/*.scss')
    .pipe(plugins.order([
      'util.scss',
      'estrutura.scss',
      '*'
    ]))
    .pipe(plugins.plumber())
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({errLogToConsole: true}))
    .pipe(plugins.concat('style.css'))
    .pipe(plugins.cleanCss({debug: true}, function (details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest(dest + 'css'));
});

gulp.task('images', function () {
  return gulp.src(src + 'images/**/*')
  /*.pipe(plugins.cache(plugins.imagemin({optimizationLevel: 5, progressive: true, interlaced: true})))*/
    .pipe(gulp.dest(dest + 'img'));
});

gulp.task('fonts', function () {
  var fontFiles = ['src/fonts/*'];
  return gulp.src(plugins.mainBowerFiles({overrides: overrides}).concat(fontFiles))
    .pipe(plugins.filter('**/*.+(eot|svg|ttf|woff|woff2|otf)'))
    .pipe(gulp.dest(dest + 'fonts'));
});
gulp.task('nunjucks', function () {
  // Gets .html and .nunjucks files in pages
  return gulp.src('src/pages/**/*.+(html|nunj)')
  // Renders template with nunjucks
    .pipe(plugins.nunjucksRender({
      path: ['templates'],
      envOptions: {
        tags: {
          blockStart: '{%',
          blockEnd: '%}',
          variableStart: '{$',
          variableEnd: '$}',
          commentStart: '{#',
          commentEnd: '#}'
        }
      }
    }))
    .pipe(gulp.dest('dist'));
});

// Watch for changes in files
gulp.task('watch', function () {
  // Watch .js files
  gulp.watch(src + 'js/*.js', ['js']);
  // Watch .scss files
  gulp.watch(src + 'css/*.scss', ['sass']);
  // Watch .css files
  gulp.watch(src + 'css/*.css', ['css']);
  // Watch .css files
  gulp.watch(src + 'fonts/**/*', ['fonts']);
  // Watch image files
  gulp.watch(src + 'images/**/*', ['images']);
  // Watch image files
  gulp.watch(src + 'pages/**/*', ['nunjucks']);
  // Watch image files
  gulp.watch('templates/**/*', ['nunjucks']);
});

// Default Task
gulp.task('default', [
  'js',
  'css',
  'sass',
  'nunjucks',
  'fonts',
  'images',
  'watch'
]);

/// Geração Scorm

gulp.task('scorm-clean', ['nunjucks', 'js', 'css', 'sass'], function () { // limpa a pasta do scorm
  return gulp.src('scorm', {read: false})
    .pipe(plugins.clean());
});

gulp.task('scorm-base', ['scorm-clean'], function () { // Copia a Base do Scorm
  return gulp.src(path_scorm_base + '/**')
    .pipe(gulp.dest('scorm'));
});

gulp.task('scorm-content', ['scorm-base'], function () { // Copia o conteúdo da aula virtual
  return gulp.src('dist/**')
    .pipe(gulp.dest('scorm/scormcontent'));
});

gulp.task('scorm-js', ['scorm-content'], function () { // Garante a utilização dos arquivos originais de js do scorm
  return gulp.src(path_scorm_base + '/scormcontent/js/**')
    .pipe(gulp.dest('scorm/scormcontent/js'));
});

gulp.task('manifest-scorm', ['scorm-js'], function () {
  return gulp.src('imsmanifest.xml')
    .pipe(gulp.dest('scorm/'));
});

gulp.task('zip-scorm', ['manifest-scorm'], function () {
  return gulp.src('scorm/**')
    .pipe(plugins.zip(scorm_name + '-scorm.zip'))
    .pipe(gulp.dest(''));
});

gulp.task('scorm', ['zip-scorm']);